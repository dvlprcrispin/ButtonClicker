package com.example.crispin.buttonclicker

class Game {
    var money = 0

    var baseIncrement = 1

    fun updateEachSecond() {
        val bonusIncrement = 4
        money += bonusIncrement + baseIncrement
    }

    fun incrementMoney() {
        money += baseIncrement
    }
}