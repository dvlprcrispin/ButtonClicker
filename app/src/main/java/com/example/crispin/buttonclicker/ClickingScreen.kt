package com.example.crispin.buttonclicker

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.TextView

private const val TAG = "ClickingScreenDebug"

class ClickingScreen : AppCompatActivity() {

    private val gameObject = Game()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_clicking_screen)

        launchIncrementer()
    }

    private fun updateLabel() {
        val label = findViewById<TextView>(R.id.ScoreDisplay)
        label.text = gameObject.money.toString()
    }


    private fun launchIncrementer() {
        val handler = Handler()

        handler.apply {
            val runner = object:Runnable {
                override fun run() {
                    runEverySecond()
                    postDelayed(this, 1000)
                }
            }
            postDelayed(runner, 1000)
        }

    }


    private fun runEverySecond() {
        gameObject.updateEachSecond()
        updateLabel()
    }



    fun onIncrementButtonPress(view: View) {
        gameObject.incrementMoney()
        updateLabel()
    }
}
